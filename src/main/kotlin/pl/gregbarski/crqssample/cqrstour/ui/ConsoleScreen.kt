package pl.gregbarski.crqssample.cqrstour.ui

open class ConsoleScreen : Screen {

    override fun start() {
        refreshScreen()
    }

    protected open fun handleInput(input: String) {}

    protected open fun showContent() {}

    protected fun handleCommandExecuted(){
        refreshScreen()
    }

    private fun refreshScreen() {
        showContent()
        handleInput(readLine()!!)
    }
}