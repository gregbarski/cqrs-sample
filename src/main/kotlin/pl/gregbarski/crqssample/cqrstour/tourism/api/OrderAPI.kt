package pl.gregbarski.crqssample.cqrstour.tourism.api

import org.axonframework.commandhandling.TargetAggregateIdentifier
import pl.gregbarski.crqssample.cqrstour.tourism.agg.OrderDetails
import java.util.*

class CreateOrderCommand(val orderId: String, val roomsLimit: Int)
class ChangeDestinationCommand(@TargetAggregateIdentifier val orderId: String, val destinationId: String)
class AddRoomCommand(@TargetAggregateIdentifier val orderId: String, val roomId: String = UUID.randomUUID().toString())
class CompleteOrderCommand(@TargetAggregateIdentifier val orderId: String)

class OrderConfirmedEvent(val orderId: String, val details: OrderDetails)
class OrderCreatedEvent(val orderId: String, val roomsLimit: Int)
abstract class OrderFormChanged(val orderId: String)
class DestinationChangedEvent(orderId: String, val destinationId: String) : OrderFormChanged(orderId)
class RoomAddedEvent(orderId: String, val roomId: String) : OrderFormChanged(orderId)

class RoomsLimitException(cause: String) : Exception()
class UnpreparedOrderException() : Exception()