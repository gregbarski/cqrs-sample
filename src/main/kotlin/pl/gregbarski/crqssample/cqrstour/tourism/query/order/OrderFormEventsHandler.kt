package pl.gregbarski.crqssample.cqrstour.tourism.query.order

import org.axonframework.eventsourcing.EventSourcingHandler
import pl.gregbarski.crqssample.cqrstour.tourism.api.DestinationChangedEvent
import pl.gregbarski.crqssample.cqrstour.tourism.api.RoomAddedEvent
import pl.gregbarski.crqssample.cqrstour.tourism.query.destination.Destination
import pl.gregbarski.crqssample.cqrstour.tourism.shared.EntityRepository

class OrderFormEventsHandler(val orderRepository: EntityRepository<OrderForm, String>,
                             val destinationsRepository: EntityRepository<Destination, String>) {

    @EventSourcingHandler
    fun on(event: DestinationChangedEvent) {
        val destination: Destination = destinationsRepository.loadBy(event.destinationId)
        val orderForm = orderRepository.loadBy(event.orderId)
        orderForm.destination = destination.name
        orderRepository.save(event.orderId, orderForm)
    }

    @EventSourcingHandler
    fun on(event: RoomAddedEvent) {
        val orderForm = orderRepository.loadBy(event.orderId)
        orderForm.roomsCount = orderForm.roomsCount + 1
        orderRepository.save(event.orderId, orderForm)
    }
}


