package pl.gregbarski.crqssample.cqrstour.ui.screens

class MenuPrinter {
    fun printMenu(options: Array<String>) {
        System.out.println("")
        for (option in options) {
            System.out.println(option)
        }
        System.out.println()
        System.out.print("Enter command: ")
    }
}