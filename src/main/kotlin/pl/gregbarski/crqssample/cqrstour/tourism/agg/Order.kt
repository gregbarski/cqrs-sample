package pl.gregbarski.crqssample.cqrstour.tourism.agg

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.model.AggregateIdentifier
import org.axonframework.commandhandling.model.AggregateLifecycle.apply
import org.axonframework.eventsourcing.EventSourcingHandler
import pl.gregbarski.crqssample.cqrstour.tourism.api.*
import java.util.*

class Order() {

    companion object{
        fun createId() = UUID.randomUUID().toString()
    }

    @AggregateIdentifier
    private var orderId: String? = null
    private var destinationId: String = ""
    private var roomsAssignment: MutableMap<String, RoomAssignment> = mutableMapOf()
    private var roomsLimit: Int = 0
    private var dateOfStay: DateOfStay = DateOfStay.EMPTY
    private var isConfirmed: Boolean = false

    @CommandHandler
    constructor(command: CreateOrderCommand) : this() {
        apply(OrderCreatedEvent(command.orderId, command.roomsLimit))
    }

    @CommandHandler
    @Throws(RoomsLimitException::class)
    fun addRoom(command: AddRoomCommand) {
        if (roomsAssignment.size + 1 > roomsLimit) {
            throw RoomsLimitException("No more rooms can be added.")
        }

        apply(RoomAddedEvent(command.orderId, command.roomId))
    }

    @CommandHandler
    fun changeDestination(command: ChangeDestinationCommand) {
        if (isConfirmed) {
            return
        }

        apply(DestinationChangedEvent(command.orderId, command.destinationId))
    }

    @CommandHandler
    fun complete(command: CompleteOrderCommand) {
        if (roomsAssignment.isEmpty()) {
            throw UnpreparedOrderException()
        }

        val details = OrderDetails(destinationId, roomsAssignment, dateOfStay)
        apply(OrderConfirmedEvent(command.orderId, details))
    }

    @EventSourcingHandler
    protected fun on(event: OrderCreatedEvent) {
        orderId = event.orderId
        roomsLimit = event.roomsLimit
    }

    @EventSourcingHandler
    protected fun on(event: DestinationChangedEvent) {
        destinationId = event.destinationId
    }

    @EventSourcingHandler
    protected fun on(event: RoomAddedEvent) {
        roomsAssignment.put(event.roomId, RoomAssignment())
    }

    @EventSourcingHandler
    protected fun on(event: OrderConfirmedEvent) {
        isConfirmed = true
    }
}

data class OrderDetails(
        val destinationId: String,
        val roomsAssignment: Map<String, RoomAssignment>,
        val dateOfStay: DateOfStay) {

    companion object {
        val EMPTY = OrderDetails("", emptyMap(), DateOfStay.EMPTY)
    }
}

