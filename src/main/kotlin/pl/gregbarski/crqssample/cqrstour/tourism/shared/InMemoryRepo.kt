package pl.gregbarski.crqssample.cqrstour.tourism.shared

open class InMemoryRepo<E, I>(val empty: E?) : EntityRepository<E, I> {
    var entities: MutableMap<I, E> = mutableMapOf()

    override fun load(): Collection<E> {
        return entities!!.values.toList()
    }

    override fun loadBy(id: I): E {
        if (entities.contains(id)) {
            return entities.get(id)!!
        }

        return empty ?: throw IllegalAccessException("No entity of id = ${id}")
    }

    override fun save(id: I, entity: E) {
        entities.put(id, entity)
    }
}

