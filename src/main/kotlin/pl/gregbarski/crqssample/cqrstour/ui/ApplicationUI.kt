package pl.gregbarski.crqssample.cqrstour.ui

import pl.gregbarski.crqssample.cqrstour.application.service.tourism.TourismAPI
import pl.gregbarski.crqssample.cqrstour.application.service.tourism.TourismApplicationService
import pl.gregbarski.crqssample.cqrstour.ui.screens.home.HomeScreen
import pl.gregbarski.crqssample.cqrstour.ui.screens.home.HomeScreen
import java.util.*

class ApplicationUI {

    private companion object {
        @JvmStatic fun main(args: Array<String>) {
            val app = ApplicationUI()
            app.showHomeScreen()
        }
    }

    private val tourismAPI: TourismAPI
    private val screens: Stack<Screen> = Stack()

    init {
        tourismAPI = TourismApplicationService()
    }

    fun showHomeScreen() {
        startNew(HomeScreen(this))
    }

    fun exit() {
        if (screens.isEmpty()) {
            System.exit(0)
        }

        screens.pop()
        runActiveScreen()
    }

    private fun startNew(newScreen: Screen) {
        screens.push(newScreen)
        runActiveScreen()
    }

    private fun runActiveScreen() {
        if (!screens.isEmpty()) {
            val screen = screens.peek()
            screen.start()
        }
    }
}