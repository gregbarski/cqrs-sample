package pl.gregbarski.crqssample.cqrstour.application

import org.axonframework.eventhandling.EventHandler

class LoggingEventsHandler {

    @EventHandler
    fun logEverything(event: Any){
        System.out.println("Event logged: " + event.javaClass.simpleName)
    }
}