package pl.gregbarski.crqssample.cqrstour.tourism.query.order

data class OrderForm(var destination: String, var roomsCount: Int) {
    companion object {
        val EMPTY = OrderForm("", 0)
    }
}