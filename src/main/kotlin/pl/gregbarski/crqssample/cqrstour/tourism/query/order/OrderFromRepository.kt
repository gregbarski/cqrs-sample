package pl.gregbarski.crqssample.cqrstour.tourism.query.order

import pl.gregbarski.crqssample.cqrstour.tourism.shared.EntityRepository

interface OrderFromRepository: EntityRepository<OrderForm, String>