package pl.gregbarski.crqssample.cqrstour.tourism.shared

interface EntityRepository<E, I> {
    fun save(id :I, entity: E)
    fun load(): Collection<E>
    fun loadBy(id: I): E
}