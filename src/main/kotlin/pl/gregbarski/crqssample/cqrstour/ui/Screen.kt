package pl.gregbarski.crqssample.cqrstour.ui

interface Screen {
    fun start()
}