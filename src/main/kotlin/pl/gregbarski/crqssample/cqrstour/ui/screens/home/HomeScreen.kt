package pl.gregbarski.crqssample.cqrstour.ui.screens.home

import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import pl.gregbarski.crqssample.cqrstour.application.service.tourism.TourismAPI
import pl.gregbarski.crqssample.cqrstour.tourism.query.destination.Destination
import pl.gregbarski.crqssample.cqrstour.ui.ApplicationUI
import pl.gregbarski.crqssample.cqrstour.ui.ConsoleScreen
import pl.gregbarski.crqssample.cqrstour.ui.screens.MenuPrinter

class HomeScreen(val tourismAPI: TourismAPI,
                 val ui: ApplicationUI,
                 val orderId: String) : ConsoleScreen() {

    override fun showContent() {
        println()
        println("=== HOME SCREEN ===")

        MenuPrinter().printMenu(arrayOf(
                "<query> - matches destinations",
                "select - confirm selection",
                "exit - return to form"))
    }

    override fun handleInput(input: String) {
        val args = input.split(" ")
        when (args[0]) {
            "exit" -> ui.exit()
            "select" -> selectDestination(args[1])
            else -> doSomething(input)
        }
    }

    private fun doSomething(name: String) {
        /*tourismAPI.autocompleteDestination(name).subscribe(object : DisposableSingleObserver<List<Destination>>() {
            override fun onSuccess(matchingDestinations: List<Destination>?) {
                destinations = matchingDestinations!!
                handleCommandExecuted()
            }

            override fun onError(error: Throwable?) {
                println(error)
                handleCommandExecuted()
            }*/
        })
    }


}