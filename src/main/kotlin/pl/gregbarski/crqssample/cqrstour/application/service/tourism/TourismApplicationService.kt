package pl.gregbarski.crqssample.cqrstour.application.service.tourism

import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.Single
import org.axonframework.commandhandling.AsynchronousCommandBus
import org.axonframework.commandhandling.CommandCallback
import org.axonframework.commandhandling.CommandMessage
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.config.Configuration
import org.axonframework.config.DefaultConfigurer
import org.axonframework.config.EventHandlingConfiguration
import org.axonframework.config.SagaConfiguration.subscribingSagaManager
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine
import pl.gregbarski.crqssample.cqrstour.application.LoggingEventsHandler
import pl.gregbarski.crqssample.cqrstour.tourism.api.*
import pl.gregbarski.crqssample.cqrstour.tourism.autocomplete.Autocomplete
import pl.gregbarski.crqssample.cqrstour.tourism.offer.HotelOfferDetails
import pl.gregbarski.crqssample.cqrstour.tourism.offer.Offer
import pl.gregbarski.crqssample.cqrstour.tourism.offer.OrderSaga
import pl.gregbarski.crqssample.cqrstour.tourism.agg.Order
import pl.gregbarski.crqssample.cqrstour.tourism.query.autocomplete.AutocompleteEventsHandler
import pl.gregbarski.crqssample.cqrstour.tourism.query.autocomplete.AutocompleteRepository
import pl.gregbarski.crqssample.cqrstour.tourism.query.destination.Destination
import pl.gregbarski.crqssample.cqrstour.tourism.query.destination.DestinationRepository
import pl.gregbarski.crqssample.cqrstour.tourism.query.offer.OfferEventsHandler
import pl.gregbarski.crqssample.cqrstour.tourism.query.offer.OfferRepository
import pl.gregbarski.crqssample.cqrstour.tourism.query.order.OrderForm
import pl.gregbarski.crqssample.cqrstour.tourism.query.order.OrderFormEventsHandler
import pl.gregbarski.crqssample.cqrstour.tourism.query.order.OrderFromRepository
import pl.gregbarski.crqssample.cqrstour.tourism.shared.InMemoryRepo
import java.time.Duration
import java.util.*

class TourismApplicationService : TourismAPI {
    private val commandGateway: CommandGateway
   // private val orderFormRepo: OrderFromRepository = object : InMemoryRepo<OrderForm, String>(OrderForm.EMPTY), OrderFromRepository {}

    init {
        val configuration = configureService()
        configuration.start()
        commandGateway = configuration.commandGateway()
    }

    private fun configureService(): Configuration {
        return DefaultConfigurer
                .defaultConfiguration()
                // Comment line below to execute commands synchronously
              //  .configureCommandBus { config -> AsynchronousCommandBus() }
              //  .configureEmbeddedEventStore { config -> InMemoryEventStorageEngine() }
              //  .configureAggregate(Order::class.java)
              //  .registerModule(subscribingSagaManager(OrderSaga::class.java))
             /*   .registerModule(EventHandlingConfiguration()
                        .registerEventHandler { config -> LoggingEventsHandler() }
                )
             */   .buildConfiguration()
    }


    fun doSomething(): Single<String> {
        return Single.create { subscriber ->
            commandGateway.send(  , object : CommandCallback<Any, Any> {
                override fun onSuccess(commandMessage: CommandMessage<out Any>?, result: Any?) {

                    subscriber.onSuccess(orderId)
                }

                override fun onFailure(commandMessage: CommandMessage<out Any>?, cause: Throwable?) {
                    subscriber.onError(cause)
                }
            })
        }
    }

    private fun createCompletableCallback(subscriber: CompletableEmitter): CommandCallback<Any, Any> {
        return object : CommandCallback<Any, Any> {
            override fun onSuccess(commandMessage: CommandMessage<out Any>?, result: Any?) {
                subscriber.onComplete()
            }

            override fun onFailure(commandMessage: CommandMessage<out Any>?, cause: Throwable?) {
                subscriber.onError(cause)
            }
        }
    }
}

